﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Unity.WebRTC;
using System;

public class PeerConnectionUnity : MonoBehaviour
{
    public RTCPeerConnection Peer;
    public Signaller signaller;
    public int RemoteId;
    public Camera cam;
    private MediaStream videoStream;
    private DelegateOnIceCandidate pc1OnIceCandidate;
    public RawImage rawImage;
    
    private RTCOfferOptions OfferOptions = new RTCOfferOptions
    {
        iceRestart = false,
        offerToReceiveAudio = false,
        offerToReceiveVideo = true
    };
    private List<RTCRtpSender> pc1Senders;

    private void Awake()
    {
        WebRTC.Initialize(EncoderType.Software);
        //WebRTC.Initialize(EncoderType.Hardware);
    }
    // Start is called before the first frame update
    void Start()
    {
        
        var configuration = GetSelectedSdpSemantics();
        Peer = new RTCPeerConnection(ref configuration);
        pc1OnIceCandidate = new DelegateOnIceCandidate(candidate => { OnAddIceCandidate(candidate); });
        Peer.OnIceCandidate = pc1OnIceCandidate;
        pc1Senders = new List<RTCRtpSender>();
        StartCoroutine(AsyncInit());
    }

    private void OnAddIceCandidate(RTCIceCandidate candidate)
    {
        Debug.Log("send candidate: " + candidate.candidate);
        signaller.SendIceCandidate(candidate.candidate, candidate.sdpMLineIndex, candidate.sdpMid);
    }

    IEnumerator AsyncInit()
    {
        if (cam)
        {
            
            /*videoStream = cam.CaptureStream(1280, 720, 1000000);
            rawImage.texture = cam.targetTexture;
            var trackcount = 0;
            foreach (var track in videoStream.GetTracks())
            {
                pc1Senders.Add(Peer.AddTrack(track));
                trackcount++;
            }
            //var mediastreams = videoStream.GetTracks();
            Debug.Log("trackcount: " + trackcount);*/
            
            //pc1Senders.Add(Peer.AddTrack(track));
            //Peer.AddTransceiver(track);
            //StartCoroutine(WebRTC.Update());
        }
        WebCamDevice[] cam_devices = WebCamTexture.devices;
        WebCamTexture webCamTexture = new WebCamTexture(cam_devices[0].name, 480, 640, 30);
        webCamTexture.Play();
        videoStream = new MediaStream();
        /*videoStream = cam.CaptureStream(1280, 720, 1000000);
        var trackcount = 0;
        foreach (var track in videoStream.GetTracks())
        {
            pc1Senders.Add(Peer.AddTrack(track, videoStream));
            trackcount++;
        }*/
        VideoStreamTrack track = new VideoStreamTrack("video", webCamTexture);
        
        videoStream.AddTrack(track);
        rawImage.texture = webCamTexture;
        //Peer.AddTrack(track, videoStream);
        pc1Senders.Add(Peer.AddTrack(track, videoStream));

        yield return new WaitForSeconds(1f);
        StartCoroutine(WebRTC.Update());
        Debug.Log("track IsInitialized: " + track.IsInitialized);
        Debug.Log("track Enabled: " + track.Enabled);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    RTCConfiguration GetSelectedSdpSemantics()
    {
        RTCConfiguration config = default;
        config.iceServers = new RTCIceServer[]
        {
            new RTCIceServer { urls = new string[] { "stun:stun.l.google.com:19302" } }
        };
        config.iceTransportPolicy = RTCIceTransportPolicy.All;
        config.bundlePolicy = RTCBundlePolicy.BundlePolicyMaxBundle;
        return config;
    }

    public void OnIceCandidate(string _candidate, string _sdpMlineIndex, string _sdpMid)
    {
        RTCIceCandidate​ candidate = new RTCIceCandidate();
        candidate.candidate = _candidate;
        candidate.sdpMid = _sdpMid;
        candidate.sdpMLineIndex = int.Parse(_sdpMlineIndex);
        Peer.AddIceCandidate(ref candidate);
        Debug.Log($"Peer ICE candidate:\n {candidate.candidate}");
    }

    public void SetRemoteDescription(string _data)
    {
        RTCSessionDescription RTCD = new RTCSessionDescription();
        RTCD.type = RTCSdpType.Answer;
        RTCD.sdp = _data;
        Peer.SetRemoteDescription(ref RTCD);
        Debug.Log("get remote sdp: " + _data);
    }

    public void CreateOffer()
    {
        StartCoroutine(CreatePeerOffer());
    }

    IEnumerator CreatePeerOffer()
    {
        var op = Peer.CreateOffer(ref OfferOptions);
        yield return op;
        StartCoroutine(OnCreateOfferSuccess(op.Desc));
        Debug.Log(op.Desc.sdp);
    }

    IEnumerator OnCreateOfferSuccess(RTCSessionDescription desc)
    {
        yield return Peer.SetLocalDescription(ref desc);
        signaller.SendSdpDataToServer(new Message
        {
            ConnectionType = "speaker",
            speakerId = RemoteId,
            Data = Convert.ToBase64String(System.Text.Encoding.UTF8.GetBytes(desc.sdp))
        });
    }

    private void OnDestroy()
    {
        
        WebRTC.Dispose();
    }
}
